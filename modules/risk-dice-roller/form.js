import {Button, Grid, TextField} from '@mui/material';
import {useState} from 'react';
import {Casino} from '@mui/icons-material';

export default function RiskDiceRollerForm(props) {
    const {onStartBattle} = props;
    const [numAttackingArmies, setNumAttackingArmies] = useState(2);
    const [numReservedAttackingArmies, setNumReservedAttackingArmies] =
        useState(2);
    const [numDefendingArmies, setNumDefendingArmies] = useState(1);

    return (
        <form
            onSubmit={onStartBattle(
                numAttackingArmies,
                numDefendingArmies,
                numReservedAttackingArmies
            )}
        >
            <Grid container direction={'column'} spacing={3}>
                <Grid item>
                    <TextField
                        value={numAttackingArmies}
                        type={'number'}
                        label={'Attacking Armies'}
                        onChange={(event) =>
                            setNumAttackingArmies(event.target.value)
                        }
                    />
                </Grid>
                <Grid item>
                    <TextField
                        value={numDefendingArmies}
                        type={'number'}
                        label={'Defending Armies'}
                        onChange={(event) =>
                            setNumDefendingArmies(event.target.value)
                        }
                    />
                </Grid>
                <Grid item>
                    <TextField
                        value={numReservedAttackingArmies}
                        type={'number'}
                        label={'Reserved Attacking Armies'}
                        onChange={(event) =>
                            setNumReservedAttackingArmies(event.target.value)
                        }
                    />
                </Grid>
                <Grid item>
                    <Button
                        variant={'contained'}
                        color={'secondary'}
                        type={'submit'}
                        fullWidth
                    >
                        <Casino/>
                        &nbsp;START BATTLE!
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
}
