import styles from './remote-support.module.css';

const RemoteSupport = () => {
    const url = '//www.teamviewer.com/link/?url=505374&id=1406914107';
    const img = {
        src: '//www.teamviewer.com/link/?url=979936&id=426248847',
        alt: 'Teamviewer for Remote Support',
    };

    return (
        <div className={styles.remoteSupport}>
            <a href={url}>
                <img
                    src={img.src}
                    alt={img.alt}
                    title={img.alt}
                    border="0"
                    width="120"
                    height="60"
                />
                <span>Remote Support</span>
            </a>
        </div>
    );
};

export default RemoteSupport;
