import {format} from 'date-fns';
import React from 'react';

const Copyright = () => {
    const copyrightYear = format(new Date(), 'yyyy');

    return <span>&copy; {copyrightYear} John Sanders</span>;
};

export default Copyright;
