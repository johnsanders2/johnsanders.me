import React from 'react';
import styles from '../styles/common-footer.module.css';
import Copyright from './copyright';
import RemoteSupport from './remote-support';

const CommonFooter = () => {
    return (
        <footer className={styles.footer}>
            <div className={styles.content}>
                <Copyright/>
                <span className={'built-with'}>
                    Built with Next.js and Vercel
                </span>
                <RemoteSupport/>
            </div>
        </footer>
    );
};

export default CommonFooter;
