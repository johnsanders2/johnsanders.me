import React from 'react';
import Head from 'next/head';

const CommonHead = (props) => {
    return (
        <Head>
            <title>{props.title}</title>
            <link rel="icon" href="/favicon.ico"/>
            <link
                rel="preload"
                href="/font/Abel/Abel-Regular.ttf"
                as="font"
                crossOrigin=""
            />
            <link
                rel="preload"
                href="/font/Roboto/Roboto-Regular.ttf"
                as="font"
                crossOrigin=""
            />
            <link
                rel="preload"
                href="/font/Roboto/Roboto-Italic.ttf"
                as="font"
                crossOrigin=""
            />
            <link
                rel="preload"
                href="/font/Roboto/Roboto-Bold.ttf"
                as="font"
                crossOrigin=""
            />
            {props.children}
        </Head>
    );
};

export default CommonHead;
