const Linkedin = () => {
    const url =
        'https://www.linkedin.com/in/john-sanders-web-developer?trk=profile-badge';

    return (
        <a className="LI-simple-link" href={url}>
            Me on LinkedIn
        </a>
    );
};

export default Linkedin;
