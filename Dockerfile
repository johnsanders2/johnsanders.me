FROM ubuntu:21.04

RUN apt-get update && apt-get install -y
COPY install-packages.sh /root/install-packages.sh
RUN chmod +x /root/install-packages.sh
RUN /root/install-packages.sh
WORKDIR /app
