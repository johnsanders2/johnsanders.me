export default class MathUtil {
    static random = (min = 1, max = 100) => {
        let num = Math.random() * max + min;

        return Math.floor(num);
    };
}
