export function isNumber(value) {
    // Since null and empty string in Number converts to 0.
    if (value === null || value === '') {
        return false;
    }
    return !isNaN(Number(value));
}

/**
 * toNumber will convert a string to a number. It will either return a number or NaN. This function is
 * intended to accept string parameters only, but other runtime values can be passed and may return unexpected
 * results. It seemed silly to return NaN when an actual number is passed at runtime so we allow that. Any other
 * type of value will simply return NaN.
 *
 * Examples:
 * '123'    => 123
 * '-123'   => -123
 * '1.234'  => 1.234
 * '0xDEAD' => 57005   // hex
 * '0b1011' => 11      // binary
 * '123e4'  => 1230000 // Be aware of this use case!
 * ''       => NaN
 * 123      => 123
 * 1.234    => 1.234
 * NaN      => NaN
 * null     => NaN
 * boolean  => NaN
 * function => NaN
 *
 * @value value string
 * @return number
 */
export function toNumber(value) {
    const valueType = typeof value;

    if (
        (valueType === 'string' && value.length > 0) ||
        valueType === 'number'
    ) {
        return Number(value);
    } else {
        return NaN;
    }
}
