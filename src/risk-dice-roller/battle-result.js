export default class BattleResult {
    _attackingArmies = 0;
    _defendingArmies = 0;

    constructor(attackingArmies, defendingArmies) {
        this._attackingArmies = attackingArmies;
        this._defendingArmies = defendingArmies;
    }

    get attackingArmies() {
        return this._attackingArmies;
    }

    set attackingArmies(value) {
        this._attackingArmies = value;
    }

    get defendingArmies() {
        return this._defendingArmies;
    }

    set defendingArmies(value) {
        this._defendingArmies = value;
    }
}
