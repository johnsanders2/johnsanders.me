import Dice from './dice';
import ArrayUtil from '../util/array-util';

export default class DiceCup {
    _diceList = [];

    constructor(numberOfDice, sidesPerDice) {
        this.bulkAdd(numberOfDice, sidesPerDice);
        return this;
    }

    bulkAdd = (numberOfDice, sidesPerDice) => {
        for (let i = 1; i <= numberOfDice; i++) {
            this._diceList.push(new Dice(sidesPerDice));
        }

        return this;
    };

    roll = (sortDirection = null) => {
        const results = [];

        for (const dice of this._diceList) {
            results.push(dice.roll());
        }

        switch (sortDirection) {
            case ArrayUtil.SORT_ASC:
                results.sort();
                break;

            case ArrayUtil.SORT_DESC:
                results.sort((a, b) => b - a);
                break;

            default:
        }

        return results;
    };
}
