import DiceCup from './dice-cup';
import ArrayUtil from '../util/array-util';
import BattleResult from './battle-result';
import {toNumber} from '../util/number-util';

export default class Battle {
    MAX_ATTACKING_ARMIES = 3;
    MIN_ATTACKING_ARMIES = 2;
    MAX_DEFENDING_ARMIES = 2;

    attackingArmies = 0;
    reserveAttackingArmies = 1;
    defendingArmies = 0;

    constructor(attackingArmies, defendingArmies, reserveAttackingArmies) {
        this.attackingArmies = toNumber(attackingArmies);
        this.defendingArmies = toNumber(defendingArmies);
        this.reserveAttackingArmies = toNumber(reserveAttackingArmies);
    }

    battle = () => {
        while (this.shouldKeepAttacking()) {
            const numAttackingDice = this.getAttackingArmies();
            const numDefendingDice = this.getDefendingArmies();

            // build a dice cup for the attacker and defender
            const attackerDiceCup = new DiceCup(numAttackingDice, 6);
            const defenderDiceCup = new DiceCup(numDefendingDice, 6);

            // roll each dice cup
            const attackerRolls = attackerDiceCup.roll(ArrayUtil.SORT_DESC);
            const defenderRolls = defenderDiceCup.roll(ArrayUtil.SORT_DESC);

            // track the losses for this battle
            let attackerLosses = 0;
            let defenderLosses = 0;

            // resolve the losses
            for (let round = 0; round <= numDefendingDice - 1; round++) {
                if (defenderRolls[round] >= attackerRolls[round]) {
                    attackerLosses--;
                } else {
                    defenderLosses--;
                }
            }
            this.attackingArmies += attackerLosses;
            this.defendingArmies += defenderLosses;
        }

        return new BattleResult(this.attackingArmies, this.defendingArmies);
    };

    shouldKeepAttacking = () =>
        this.attackingArmies > this.reserveAttackingArmies &&
        this.defendingArmies > 0 &&
        this.attackingArmies >= this.MIN_ATTACKING_ARMIES;

    getAttackingArmies = () =>
        Math.min(this.MAX_ATTACKING_ARMIES, this.attackingArmies - 1);
    getDefendingArmies = () =>
        Math.min(this.MAX_DEFENDING_ARMIES, this.defendingArmies);
}
