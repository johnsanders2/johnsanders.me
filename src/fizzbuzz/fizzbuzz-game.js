export default class FizzBuzzGame {
    _rounds = 10;
    _items = [];
    _results = [];

    constructor(rounds, items) {
        this._rounds = rounds;
        this._items = items;
    }

    get rounds() {
        return this._rounds;
    }

    set rounds(value) {
        this._rounds = value;

        return this;
    }

    get items() {
        return this._items;
    }

    set items(value) {
        this._items = value;
    }

    get results() {
        return this._results;
    }

    play() {
        const start = 1;
        const end = this._rounds;

        for (let roundNum = start; roundNum <= end; roundNum++) {
            let results = [];

            this._items.forEach((item) => {
                if (roundNum % item.number === 0) {
                    results.push(item.name);
                }
            });

            if (results.length === 0) results.push(roundNum);
            this._results.push(results.join('-'));
        }

        return this;
    }
}
