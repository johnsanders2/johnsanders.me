export default class FizzBuzzItem {
    _number = 0;
    _name = '';

    constructor(number, name) {
        this._number = number;
        this._name = name;
    }

    get number() {
        return this._number;
    }

    set number(value) {
        this._number = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }
}
