This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

I've included my own Dockerfile so you can run this project as a container. I don't like installing all of the dependencies required to run these things, so the docker container works nicely, albeit a little bit slow.

First off, build the project using Docker:

```bash
docker build -t johnsanders.me .
```

Then run a bash shell into the container, linking in the project folder and allowing the web server to host outside the container

```bash
docker run -it --rm -v $(pwd):/app -p 3000:3000 johnsanders.me bash
```

Finally, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.
