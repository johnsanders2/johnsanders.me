import Link from 'next/link';
import styles from '../styles/Home.module.css';
import {Casino, Description, EmojiNature, LinkedIn} from '@mui/icons-material';
import {Grid, List, ListItem} from '@mui/material';
import CommonHead from '../modules/common-head';
import CommonFooter from '../modules/common-footer';

export default function Home() {
    return (
        <div className={styles.container}>
            <CommonHead title={'John Sanders - Sr. Software Developer'}/>

            <main className={styles.main}>
                <h1 className={styles.title}>John Sanders</h1>
                <h2 className={styles.description}>
                    Principal Architect focusing on React/Typescript front-ends
                </h2>

                <Grid container direction="row" justify="center" alignItems="center" justifyContent={'space-evenly'}>
                    <List component="nav" aria-labelledby="nested-list-subheader"
                          subheader={<h3> Things about me... </h3>}>
                        <ListItem>
                            <Description/>
                            <Link href={'/resume'}>Resume</Link>
                        </ListItem>
                        <ListItem>
                            <LinkedIn/>
                            <Link
                                href={
                                    'https://www.linkedin.com/in/john-sanders-web-developer?trk=profile-badge'
                                }
                            >
                                LinkedIn
                            </Link>
                        </ListItem>
                    </List>
                    <List component="nav" aria-labelledby="nested-list-subheader"
                          subheader={<h3> Just for fun... </h3>}>
                        <ListItem>
                            <EmojiNature/>
                            <Link href={'/fizzbuzz'}>Fizzbuzz</Link>
                        </ListItem>
                        <ListItem>
                            <Casino/>
                            <Link href={'/risk-dice-roller'}>
                                Risk Dice Roller
                            </Link>
                        </ListItem>
                    </List>
                </Grid>
            </main>

            <CommonFooter/>
        </div>
    );
}
