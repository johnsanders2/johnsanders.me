import styles from '../styles/Home.module.css';
import {Grid} from '@mui/material';
import FizzBuzzItem from '../src/fizzbuzz/fizzbuzz-item';
import FizzBuzzGame from '../src/fizzbuzz/fizzbuzz-game';
import CommonHead from '../modules/common-head';
import CommonFooter from '../modules/common-footer';

export default function Fizzbuzz() {
    const fizzbuzzItems = [
        new FizzBuzzItem(5, 'Fizz'),
        new FizzBuzzItem(7, 'Buzz'),
    ];

    const fizzbuzzGame = new FizzBuzzGame(100, fizzbuzzItems);

    fizzbuzzGame.play();

    const fizzbuzzItemsForDisplay = fizzbuzzItems.map(
        (item) => `${item.number} = ${item.name}`
    );

    return (
        <div className={styles.container}>
            <CommonHead title={'Fizzbuzz - John Sanders'}/>

            <main className={styles.main}>
                <h1>Fizzbuzz Game</h1>
                <h2>
                    This is a hard coded display of a game.
                </h2>
                <h2>
                    Legend: <small>{fizzbuzzItemsForDisplay.join(', ')}</small>
                </h2>
                <hr/>
                <Grid container direction={'column'}>
                    <h2>Results:</h2>
                    {fizzbuzzGame.results.map((result) => {
                        return <span>{result}</span>;
                    })}
                </Grid>
            </main>

            <CommonFooter/>
        </div>
    );
}
