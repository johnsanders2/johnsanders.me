import styles from '../styles/resume.module.css';
import React, { useState } from 'react';
import { EmailOutlined, Https, LinkedIn, OpenInNew, PhoneIphoneOutlined, PinDropOutlined } from '@mui/icons-material';
import Link from 'next/link';
import CommonHead from '../modules/common-head';

const Resume = () => {
    const defaultPhoneLabel = 'Click to see #';
    const [phone, setPhone] = useState(defaultPhoneLabel);
    const [phoneClass, setPhoneClass] = useState();
    const handlePhoneClick = () => {
        setPhone('+1 925-577-1866');
        setPhoneClass(styles.shown);
    };

    const defaultEmailLabel = 'Click to see email';
    const [email, setEmail] = useState(defaultEmailLabel);
    const [emailClass, setEmailClass] = useState();
    const handleEmailClick = () => {
        setEmail('john@johnsanders.me');
        setEmailClass(styles.shown);
    };

    return (
        <>
            <CommonHead title={'John Sanders - Sr. Software Developer'} />

            <div className={styles.resume}>
                <header className={styles.header}>
                    <h1 className={styles.name}>John Sanders</h1>
                    <h2 className={styles.title}>Sr. Software Developer</h2>
                </header>

                <div className={styles.container}>
                    <aside className={styles.leftColumn}>
                        <section>
                            <h2 className={[styles.sectionTitle, styles.cropTop].join(' ')}>Contact</h2>
                            <ul className={styles.contactInfo}>
                                <li className={styles.mobile}>
                                    <PhoneIphoneOutlined fontSize={'large'} className={styles.icon} />
                                    <a className={[styles.phone, phoneClass].join(' ')} onClick={handlePhoneClick}>
                                        {phone}
                                    </a>
                                </li>
                                <li className={styles.email}>
                                    <EmailOutlined fontSize={'large'} className={styles.icon} />
                                    <a className={[styles.email, emailClass].join(' ')} onClick={handleEmailClick}>
                                        {email}
                                    </a>
                                </li>
                                <li className={styles.location}>
                                    <PinDropOutlined fontSize={'large'} className={styles.icon} />
                                    <a href="https://www.google.com/search?q=Plano,+TX" target="_blank" rel="noopener">
                                        Plano, Texas
                                    </a>
                                </li>
                                <li className={styles.linkedIn}>
                                    <LinkedIn fontSize={'large'} className={styles.icon} />
                                    <a
                                        href="https://linkedin.com/in/john-sanders-web-developer"
                                        target="_blank"
                                        rel="noopener"
                                    >
                                        john-sanders-web-developer
                                    </a>
                                </li>
                                <li className={styles.website}>
                                    <Https fontSize={'large'} className={styles.icon} />
                                    <Link href="/">johnsanders.me</Link>
                                </li>
                            </ul>
                        </section>
                        <section>
                            <h2 className={styles.sectionTitle}>Skills</h2>

                            <div className={styles.skillList}>
                                <h3 className={styles.caption}>Professional</h3>
                                <ul className={styles.skills}>
                                    <li>Full stack web development</li>
                                    <li>UI/UX design</li>
                                    <li>Traditional and cloud server architecture and administration</li>
                                    <li>AWS Architecture /development</li>
                                    <li>Database design</li>
                                    <li>Process and workflow optimization</li>
                                </ul>
                            </div>
                            <div className={styles.skillList}>
                                <h3 className={styles.caption}>Backend Competencies</h3>
                                <ul className={styles.skills}>
                                    <li>PHP, Java/JSP, Python, SQL</li>
                                    <li>Laravel</li>
                                    <li>Joomla, Wordpress</li>
                                    <li>Nginx, Apache, Tomcat</li>
                                    <li>TLS, Let’s Encrypt</li>
                                    <li>MySQL, DB2, Oracle</li>
                                </ul>
                            </div>
                            <div className={styles.skillList}>
                                <h3 className={styles.caption}>Frontend Competencies</h3>
                                <ul className={styles.skills}>
                                    <li>Typescript</li>
                                    <li>React, Redux, AngularJS, Moment</li>
                                    <li>Vanilla Javascript / ES6</li>
                                    <li>HTML</li>
                                    <li>CSS, CSS3, LESS, SASS, Bootstrap</li>
                                    <li>SVG, Font icons, Web Fonts</li>
                                    <li>Webpack, Gulp</li>
                                </ul>
                            </div>
                            <div className={styles.skillList}>
                                <h3 className={styles.caption}>Cloud Competencies</h3>
                                <p className={styles.subheading}>Amazon Web Services (AWS)</p>
                                <ul className={styles.skills}>
                                    <li>Elastic Cloud Computing (EC2)</li>
                                    <li>CloudFront (CDN)</li>
                                    <li>Simple Storage Service (S3)</li>
                                    <li>Relational Database Service (RDS)</li>
                                    <li>Lambda, EFS, API Gateway</li>
                                </ul>
                            </div>
                        </section>
                        <section>
                            <h2 className={styles.sectionTitle}>Community</h2>

                            <div className={styles.skillList}>
                                <h3 className={styles.caption}>Open Source Contributor</h3>
                                <ul className={styles.skills}>
                                    <li>
                                        <a href="https://github.com/CodeSeven/toastr" target="_blank" rel="noopener">
                                            CodeSeven/toastr
                                        </a>
                                    </li>
                                    <li>
                                        <a
                                            href="https://github.com/lastpass/joomla-saml"
                                            target="_blank"
                                            rel="noopener"
                                        >
                                            lastpass/jooma-saml
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className={styles.skillList}>
                                <h3 className={styles.caption}>Board of Directors</h3>
                                <strong>The Community of Caring Hearts Charity</strong>
                                <a className={styles.link} href={'https://caringheartscharity.org'} target={'_blank'}>
                                    caringheartscharity.org <OpenInNew fontSize={'inherit'} />
                                </a>
                                IT Director
                                <br />
                                2013 &mdash; Present
                                <br />
                                <br />
                            </div>
                            <div className={styles.skillList}>
                                <h3 className={styles.caption}>Leadership</h3>
                                <strong>Thelab.ms - Everyone's Makerspace</strong>
                                <a className={styles.link} href={'https://thelab.ms'} target={'_blank'}>
                                    thelab.ms <OpenInNew fontSize={'inherit'} />
                                </a>
                                Facilities Coordinator
                                <br />
                                2018 &mdash; 2022
                            </div>
                        </section>
                    </aside>
                    <article className={styles.jobHistory}>
                        <section>
                            <h2 className={[styles.sectionTitle, styles.cropTop].join(' ')}>Professional Profile</h2>
                            <p>
                                Personally and professionally, my interests lie in technology and web development. I
                                enjoy the challenges that come with problem solving, troubleshooting, and developing
                                stable and secure IT infrastructure and software architecture that meets business needs.
                                As a web developer, I love learning and applying new technologies to the greatest extent
                                possible, and I use this knowledge to fuel the success of every project I work on.{' '}
                            </p>
                        </section>
                        <section>
                            <h2 className={styles.sectionTitle}>Professional Experience</h2>

                            <div className={styles.job}>
                                <h3 className={styles.title}>Sr. Software Developer</h3>
                                <ul className={styles.info}>
                                    <li className={styles.companyName}>Advance Auto Parts, Inc.</li>
                                    <li className={styles.location}>Plano, TX</li>
                                    <li className={styles.yearsWorked}>2018 &mdash; Present</li>
                                </ul>
                                <ul className={styles.achievements}>
                                    <li>
                                        Rebuild legacy Java Swing client using modern, responsive web technologies
                                        (Typescript, ReactJS, Redux, SCSS, Webpack), targeting platforms such as desktop
                                        (Window/Mac), tablets and mobile (iOS and Android)
                                    </li>
                                    <li>
                                        Build responsive and flexible interfaces using media queries, Flex/Grid systems,
                                        Material UI, and Bootstrap.
                                    </li>
                                    <li>
                                        Develop custom reusable framework UI components, when off-the-shelf options
                                        aren't feasible
                                    </li>
                                    <li>Integrate web client with REST API using Axios, promises, async/await</li>
                                    <li>
                                        Custom Redux middleware for global error handling, and authentication checks
                                    </li>
                                    <li>Web workers utilized for reliable timekeeping</li>
                                    <li>
                                        Automate static code analysis with ESLint, and code formatting with Prettier
                                    </li>
                                    <li>Build and bundle web application using Webpack, and various plugins</li>
                                    <li>Code repositories: Git (Bitbucket), SVN</li>
                                    <li>Git branching strategy for feature/bug fixes, and releases</li>
                                    <li>Build and run Docker container for reusable local development environments</li>
                                    <li>
                                        Automate application build and run in OpenShift (IBM's Kubernetes platform), on
                                        both IBMi hardware and AWS (ROSA)
                                    </li>
                                    <li>
                                        Utilize CI/CD strategies, including BitBucket webhooks and Docker application
                                        container builds for rapid changes with automated deployment to development
                                        environment
                                    </li>
                                    <li>
                                        Review pull requests, ensuring code standards are followed, quality checks are
                                        made
                                    </li>
                                    <li>
                                        Perform end to end UI testing in Browserstack across all supported platforms
                                    </li>
                                </ul>
                            </div>
                            <div className={styles.job}>
                                <h3 className={styles.title}>Web Applications Developer</h3>
                                <ul className={styles.info}>
                                    <li className={styles.companyName}>WORLDPAC, Inc.</li>
                                    <li className={styles.location}>Plano, TX</li>
                                    <li className={styles.yearsWorked}>2012 &mdash; 2018</li>
                                </ul>
                                <ul className={styles.achievements}>
                                    <li>Web application development lead.</li>
                                    <li>
                                        Create and maintain AWS Cloud Architecture (EC2, EFS, S3, CDN, RDS, Lambda).
                                        Turnkey web hosting platforms built with code.
                                    </li>
                                    <li>
                                        Migrate internal legacy product imaging service to AWS: Service Management web
                                        application utilizes PHP7 and Laravel 5.5, and beanstalkd for queue management.
                                        Create automated image processing workflow with S3, API Gateway, Lambda, and
                                        CloudFront.
                                    </li>
                                    <li>
                                        Create and implement development tools (node.js, gulp with plugins) to build web
                                        applications for deployment to staging and production environments.
                                    </li>
                                    <li>
                                        Troubleshoot Javascript implementations and assist with certification process
                                        for third-party web-based vendors that integrate with a Java client WebSocket
                                        API.
                                    </li>
                                    <li>
                                        Designed and developed workflow efficiency web application that helped increase
                                        employee productivity by 52%.
                                    </li>
                                    <li>
                                        Enhance and modernize legacy e-commerce website. Project includes:
                                        <ul>
                                            <li>
                                                Transform monolithic JSP page architecture to a JSON API backend with an
                                                AngularJS frontend.
                                            </li>
                                            <li>
                                                Implement DRY and Single Responsibility principles to simplify the code
                                                base.
                                            </li>
                                            <li>
                                                Reduce server traffic by implementing smaller, targeted AJAX data
                                                requests, and combining and minifying JS/CSS.
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        Implement Amazon CloudFront Content Delivery Network reducing server traffic by
                                        75%, improving server uptime to 99.92%.
                                    </li>
                                    <li>
                                        Joomla website development, including custom templates, WAF
                                        installation/maintenance, and automated backups to S3.
                                    </li>
                                </ul>
                            </div>

                            <div className={styles.job}>
                                <h3 className={styles.title}>Webmaster</h3>
                                <ul className={styles.info}>
                                    <li className={styles.companyName}>WORLDPAC, Inc.</li>
                                    <li className={styles.location}>Newark, CA</li>
                                    <li className={styles.yearsWorked}>2010 &mdash; 2012</li>
                                </ul>
                                <ul className={styles.achievements}>
                                    <li>
                                        Designed, developed, and maintained over 30 public and intranet web properties.
                                    </li>
                                    <li>
                                        Enhanced the look and performance of an in-house reporting tool. Entire UI
                                        refresh. Database and application performance enhancements reduced average page
                                        load time from fifteen seconds to less than one second.
                                    </li>
                                    <li>
                                        Designed and developed a Polldaddy scheduling and publishing tool. Provided the
                                        Marketing department with a single, easy to use, self-service tool to manage
                                        their Polldaddy campaigns across various web properties.
                                    </li>
                                    <li>
                                        Developed and maintained geolocation web services: map visualizations, customer
                                        training class locator, precise email campaign targeting.
                                    </li>
                                </ul>

                                <h3 className={styles.title}>Freelance Web Developer/Designer</h3>
                                <ul className={styles.info}>
                                    <li className={styles.companyName}>johnsanders.me</li>
                                    <li className={styles.location}>San Ramon, CA</li>
                                    <li className={styles.yearsWorked}>2008 &mdash; 2010</li>
                                </ul>
                                <ul className={styles.achievements}>
                                    <li>
                                        Managed entire customer lifecycle including assessing and documenting client
                                        requirements, developing solutions, and ensuring customer success.
                                    </li>
                                    <li>Designed and developed custom websites, micro-frameworks and templates.</li>
                                    <li>
                                        Converted existing websites into content management systems, such as TextPattern
                                        and Joomla, providing customers with the capability to edit their own website
                                        content.
                                    </li>
                                    <li>Developed Javascript/jQuery user interfaces.</li>
                                </ul>
                            </div>

                            <div className={styles.job}>
                                <h3 className={styles.title}>Senior IT Analyst</h3>
                                <ul className={styles.info}>
                                    <li className={styles.companyName}>AT&T Services</li>
                                    <li className={styles.location}>San Ramon, CA</li>
                                    <li className={styles.yearsWorked}>1996 &mdash; 2008</li>
                                </ul>
                                <ul className={styles.achievements}>
                                    <li>
                                        Managed development and implementation of Automatic Call Distributors (ACD).
                                    </li>
                                    <li>
                                        Developed XHTML/CSS front-end web interfaces for software configuration
                                        utilities.
                                    </li>
                                    <li>
                                        Coded utility applications in VBScript, manipulating file systems, Windows
                                        registry, and creating run-time logs.
                                    </li>
                                    <li>Designed and built Oracle and Access databases.</li>
                                    <li>
                                        Built reporting server repository. Developed reports and automated distribution
                                        of executive reports.
                                    </li>
                                    <li>
                                        Administered Crystal Reports systems, including installation, configuration, and
                                        custom report creation.
                                    </li>
                                    <li>Ensured accurate billing by careful review of vendor maintenance contracts.</li>
                                    <li>Trained and mentored team members.</li>
                                </ul>
                            </div>
                        </section>
                    </article>
                </div>
            </div>
        </>
    );
};

export default Resume;
