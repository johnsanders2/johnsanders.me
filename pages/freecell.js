import styles from '../styles/freecell.module.scss';

export default function Freecell() {
    return (
        <div className={styles.container}>
            <main className={styles.board}>
                <div className={styles.stacks}>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                </div>
                <div className={styles.tableaus}>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                </div>
                <div className={styles.freeCells}>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                    <div className={styles.cardSlot}></div>
                </div>
            </main>
        </div>
    );
}
