import styles from '../styles/Home.module.css';
import Battle from '../src/risk-dice-roller/battle';
import {useState} from 'react';
import RiskDiceRollerForm from '../modules/risk-dice-roller/form';
import {Card, CardContent} from '@mui/material';
import CommonHead from '../modules/common-head';
import CommonFooter from '../modules/common-footer';

export default function RiskDiceRoller() {
    const [battleResult, setBattleResult] = useState(null);

    const handleBattle =
        (numAttackingArmies, numDefendingArmies, numReserveAttackingArmies) =>
            (event) => {
                event.stopPropagation();
                event.preventDefault();

                // eslint-disable-next-line no-console
                console.log(
                    numAttackingArmies,
                    numDefendingArmies,
                    numReserveAttackingArmies
                );

                const battle = new Battle(
                    numAttackingArmies,
                    numDefendingArmies,
                    numReserveAttackingArmies
                );
                const battleResult = battle.battle();

                // eslint-disable-next-line no-console
                console.log('battleResult', battleResult);

                setBattleResult(battleResult);
            };

    const maybeDisplayResults = () => {
        if (!battleResult) return null;

        return (
            <>
                <h2>Results:</h2>
                <p>
                    Remaining Attacking Armies: {battleResult.attackingArmies}
                </p>
                <p>
                    Remaining Defending Armies: {battleResult.defendingArmies}
                </p>
            </>
        );
    };

    return (
        <div className={styles.container}>
            <CommonHead title={'Risk Dice Roller - John Sanders'}/>

            <main className={styles.main}>
                <Card>
                    <CardContent>
                        <RiskDiceRollerForm onStartBattle={handleBattle}/>
                    </CardContent>
                </Card>
                {maybeDisplayResults()}
            </main>

            <CommonFooter/>
        </div>
    );
}
